from pylab import *
#matplotlib inline
caffe_root = '/opt/caffe/'  # this file should be run from {caffe_root}/examples (otherwise change this line)

import sys
sys.path.insert(0, caffe_root + 'python')
import caffe

import os
from caffe import layers as L, params as P
from AccuracyUtil import *

def conduct_test(solver, current_iteration, number_of_test_iteration, output_file_name, pure_stats_file_name):

	output_file = open(output_file_name, 'a')
	pure_stats = open(pure_stats_file_name, 'a')
	accuracyUtil = AccuracyUtil()
	print 'test starts at the iteration: ', current_iteration
	print 'actual\tpredicted\tmax\tSG\tCH\tAR\tFL\tQS\n'
	for x in range(0, number_of_test_iteration):
		
		solver.test_nets[0].forward()  # test net (there can be more than one)

		labels = solver.test_nets[0].blobs['label'].data.astype(int)
		predictions = solver.test_nets[0].blobs['prob'].data.argmax(1)
		probs = solver.test_nets[0].blobs['prob'].data

		labelMap = {0:'AR', 1:'CH', 2:'SG', 3:'FL', 4:'QS'}

		for l, p, pp in zip(labels, predictions, probs):
			if l == p:
				accuracyUtil.addToRight(labelMap[l])
			else:
				accuracyUtil.addToWrong(labelMap[l], labelMap[p])
			output_file.write(labelMap[l] + '\t' + labelMap[p] + '\t' + str(pp[p]) + '\t' + str(pp[0]) + '\t' + str(pp[1]) + '\t' + str(pp[2]) + '\t' + str(pp[3]) + '\t' + str(pp[4]) + '\n') 
		accuracy = str(sum(solver.test_nets[0].blobs['label'].data == solver.test_nets[0].blobs['prob'].data.argmax(1)))
		output_file.write('accuracy : ' + accuracy + '\n')

	pure_stats.write(str(current_iteration) + '\t' + accuracyUtil.getPureAccuracy() + '\n')
	output_file.close()
	pure_stats.close()
	accuracyUtil.printAccuracy()

def main(argv):

	#
	#SAMPLE RUN -> nohup python run_model.py "/home/ahmet/workspace/solarnet/models/final/alexnet/default/small/256_solver.prototxt" pure_results/small_32_ 2000 100 4 > results/small_256_alexnet_iteration_1000_batch_256_50.txt 2>&1 &
	if len(argv) < 5:
		print 'missing argument: ARGS -> solver.prototxt, output_file, # of Iteration, frequency of test, # of Test Iteration'

	caffe.set_mode_cpu()
	solver_path = argv[0]
	output_file_name = argv[1] + "_result.txt"
	pure_stats_file_name = argv[1] + "_pure_stats.txt"
	number_of_iteration = int(argv[2])
	freq_of_test = int(argv[3])
	number_of_test_iteration = int(argv[4])

	print 'Started with: ', solver_path, ' iteration: ', number_of_iteration, ' test iteration: ', number_of_test_iteration

	### load the solver and create train and test nets
	#solver = None  # ignore this workaround for lmdb data (can't instantiate two solvers on the same data)
	solver = caffe.SGDSolver(solver_path)

	# each output is (batch size, feature dim, spatial dim)
	for k, v in solver.net.blobs.items():
	    print (k, v.data.shape)

	# just print the weight sizes (we'll omit the biases)
	for k, v in solver.net.params.items():
	    print (k, v[0].data.shape)

	number_of_steps = int(number_of_iteration / freq_of_test)
	for x in range(0, number_of_steps+1):
		conduct_test(solver, freq_of_test*x, number_of_test_iteration, output_file_name, pure_stats_file_name)
		solver.step(freq_of_test)  # train net


	#print 'accuracy : ', str(sum(solver.test_nets[0].blobs['label'].data == solver.test_nets[0].blobs['ip2'].data.argmax(1))) 

	##imshow(solver.test_nets[0].blobs['data'].data[:8, 0].transpose(1, 0, 2).reshape(28, 8*28), cmap='gray'); axis('off')
	#print 'test labels:', solver.test_nets[0].blobs['label'].data[:]

if __name__ == "__main__":
	main(sys.argv[1:])
