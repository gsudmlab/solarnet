def addToRight(eventType, eventByRight):
    if eventType in eventByRight:
        eventByRight[eventType] = eventByRight[eventType] + 1
    else:
        eventByRight[eventType] = 1

def addToWrong(eventType, eventByWrong):
    if eventType in eventByWrong:
        eventByWrong[eventType] = eventByWrong[eventType] + 1
    else:
        eventByWrong[eventType] = 1
            


def getAccuracy(list):
    counter = 0.0
    eventByWrong = {}
    eventByRight = {}
    for i in range(0, len(list)):
        if list[i][0] != list[i][1]:
            counter = counter + 1
            addToWrong(list1[i][0], eventByWrong)
        else:
            addToRight(list1[i][0], eventByRight)
    print eventByWrong
    print eventByRight
    return (counter / len(newList)) * 100

def getProbsFromFile(fileName):
    listOfProbs = []
    with open(fileName, "r") as ins:

        for line in ins:
            tuples = line.split('\t')
            if len(tuples) > 3:
                listOfProbs.append(tuples[:3])
    return listOfProbs
        	
list1 = getProbsFromFile("large_256_googlenet_iteration_1000_batch_32_50_probs.txt")
list2 = getProbsFromFile("large_256_alexnet_iteration_1000_batch_256_50_probs.txt")

newList = []
for i in range(0, len(list1)):
    if list1[i][0] != list2[i][0]:
        print "wrong input"
    if float(list1[i][2]) > float(list2[i][2]):
        newList.append(list1[i])
    else:
        newList.append(list2[i])

print "error rate list1: ", getAccuracy(list1)
print "error rate list2: ", getAccuracy(list2)
print "error rate newList: ", getAccuracy(newList)