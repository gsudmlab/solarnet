from pylab import *
#matplotlib inline
caffe_root = '/opt/caffe/'  # this file should be run from {caffe_root}/examples (otherwise change this line)

import sys
sys.path.insert(0, caffe_root + 'python')
import caffe

import os
from caffe import layers as L, params as P
from AccuracyUtil import *

def main(argv):

	if len(argv) < 2:
		print 'missing argument: ARG1 -> solver.prototxt ARG3 -> # of Iteration ARG3 -> # of Test Iteration'

	caffe.set_mode_cpu()
	solver_path = argv[0]
	number_of_iteration = int(argv[1])
	number_of_test_iteration = int(argv[2])

	print 'Started with: ', solver_path, ' iteration: ', number_of_iteration, ' test iteration: ', number_of_test_iteration

	### load the solver and create train and test nets
	#solver = None  # ignore this workaround for lmdb data (can't instantiate two solvers on the same data)
	solver = caffe.SGDSolver(solver_path)

	# each output is (batch size, feature dim, spatial dim)
	for k, v in solver.net.blobs.items():
	    print (k, v.data.shape)

	# just print the weight sizes (we'll omit the biases)
	for k, v in solver.net.params.items():
	    print (k, v[0].data.shape)

	solver.step(number_of_iteration)  # train net

	accuracyUtil = AccuracyUtil()
	print 'actual\tpredicted\tmax\tSG\tCH\tAR\tFL\tQS\n'
	for x in range(0, number_of_test_iteration):
		print 'test starts'
		solver.test_nets[0].forward()  # test net (there can be more than one)
		
		# we use a little trick to tile the first eight images
		#imshow(solver.net.blobs['data'].data[:8, 0].transpose(1, 0, 2).reshape(28, 8*28), cmap='gray'); axis('off')
		#print 'actual labels:', solver.net.blobs['label'].data.astype(int)
		#print 'predicted labels:', solver.net.blobs['prob'].data.argmax(1)

		labels = solver.test_nets[0].blobs['label'].data.astype(int)
		predictions = solver.test_nets[0].blobs['prob'].data.argmax(1)
		probs = solver.test_nets[0].blobs['prob'].data

		labelMap = ['SG', 'CH', 'AR', 'FL','QS']

		for l,p, pp in zip(labels,predictions, probs):
			if l == p:
				accuracyUtil.addToRight(labelMap[l])
			else:
				accuracyUtil.addToWrong(labelMap[l], labelMap[p])
			print labelMap[l], '\t', labelMap[p], '\t', pp[p], '\t', pp[0], '\t', pp[1], '\t', pp[2], '\t', pp[3], '\t', pp[4]

		print 'accuracy : ', str(sum(solver.test_nets[0].blobs['label'].data == solver.test_nets[0].blobs['prob'].data.argmax(1)))
	accuracyUtil.printAccuracy()
	#print 'accuracy : ', str(sum(solver.test_nets[0].blobs['label'].data == solver.test_nets[0].blobs['ip2'].data.argmax(1))) 

	##imshow(solver.test_nets[0].blobs['data'].data[:8, 0].transpose(1, 0, 2).reshape(28, 8*28), cmap='gray'); axis('off')
	#print 'test labels:', solver.test_nets[0].blobs['label'].data[:]

if __name__ == "__main__":
	main(sys.argv[1:])
