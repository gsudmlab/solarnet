import caffe
import numpy as np
from PIL import Image
import sys

# take an array of shape (n, height, width) or (n, height, width, channels)
#  and visualize each (height, width) thing in a grid of size approx. sqrt(n) by sqrt(n)
def vis_square(data, padsize=1, padval=0):
    data -= data.min()
    data /= data.max()
    
    # force the number of filters to be square
    n = int(np.ceil(np.sqrt(data.shape[0])))
    padding = ((0, n ** 2 - data.shape[0]), (0, padsize), (0, padsize)) + ((0, 0),) * (data.ndim - 3)
    data = np.pad(data, padding, mode='constant', constant_values=(padval, padval))
    
    # tile the filters into an image
    data = data.reshape((n, n) + data.shape[1:]).transpose((0, 2, 1, 3) + tuple(range(4, data.ndim + 1)))
    data = data.reshape((n * data.shape[1], n * data.shape[3]) + data.shape[4:])
    return data

project_root = "/home/ahmet/workspace/solarnet/models/"

models = [[project_root + "rerun/alexnet/train_val_256.prototxt", project_root + "snapshots/rerun/alexnet/256/_iter_4500.caffemodel", "alexnet"],[project_root + "rerun/cifar/train_val_128.prototxt", project_root + "snapshots/rerun/cifar/128/_iter_5000.caffemodel", "cifar"],[project_root + "rerun/mnist/train_test_32.prototxt", project_root + "snapshots/rerun/mnist/32/_iter_5000.caffemodel", "mnist"]]

def main(args):

	i = int(args[0])
	layer = args[1]
	m = models[i]

	caffe.set_mode_cpu()
	net = caffe.Net(m[0], m[1], caffe.TEST)

	filters = net.params[layer][0].data
	print(filters[0].shape)
	A = vis_square(filters.transpose(0,2,3,1))
	print(A.shape)
	im = Image.fromarray(np.uint8(A*255))
	im.save(m[2] + "_" + layer + ".png")
	print(A[0])

if __name__ == "__main__":
	main(sys.argv[1:])
