import caffe
import sys

project_root = "/home/ahmet/workspace/solarnet/models/"

models = [[project_root + "final/mnist/large/test_32.prototxt", project_root + "snapshots/mnist/large/_iter_5000.caffemodel"],
[project_root + "final/mnist/mid/test_32.prototxt", project_root + "snapshots/mnist/mid/_iter_5000.caffemodel"],
[project_root + "final/mnist/small/test_32.prototxt", project_root + "snapshots/mnist/small/_iter_5000.caffemodel"],

[project_root + "final/cifar/large/test_128.prototxt", project_root + "snapshots/cifar/128_large/_iter_1000.caffemodel"],
[project_root + "final/cifar/mid/test_128.prototxt", project_root + "snapshots/cifar/128_mid/_iter_1000.caffemodel"],
[project_root + "final/cifar/small/test_128.prototxt", project_root + "snapshots/cifar/128_small/_iter_1000.caffemodel"],

[project_root + "final/alexnet/default/large/test_256.prototxt", project_root + "snapshots/alexnet/256_large/_iter_1000.caffemodel"],
[project_root + "final/alexnet/default/mid/test_256.prototxt", project_root + "snapshots/alexnet/256_mid_iter_1000.caffemodel"],
[project_root + "final/alexnet/default/small/test_256.prototxt", project_root + "snapshots/alexnet/256_small/_iter_1000.caffemodel"]]
'''project_root + "final/cifar/small/test_128.prototxt", project_root + "snapshots/cifar/128_small/_iter_1000.caffemodel"]]
		[project_root + "final/alexnet/default/large/train_val_256.prototxt", project_root + "snapshots/alexnet/256_large/_iter_5000.caffemodel"],
		[project_root + "final/alexnet/default/mid/train_val_256.prototxt", project_root + "snapshots/alexnet/256_mid/_iter_5000.caffemodel"],
		[project_root + "final/alexnet/default/small/train_val_256.prototxt", project_root + "snapshots/alexnet/256_small/_iter_5000.caffemodel"],
        [project_root + "final/cifar/large/train_val_128.prototxt", project_root + "snapshots/cifar/256_large/_iter_5000.caffemodel"],
        [project_root + "final/cifar/large/train_val_128.prototxt", project_root + "snapshots/cifar/256_large/_iter_5000.caffemodel"],]
'''
def test_for_model(net):
	total_acc = 0
	for x in range(0, 15):
		net.forward()  # test net (there can be more than one)

		labels = net.blobs['label'].data.astype(int)
		predictions = net.blobs['prob'].data.argmax(1)
		probs = net.blobs['prob'].data

		accuracy = sum(net.blobs['label'].data == net.blobs['prob'].data.argmax(1))
		print("iteration " + str(x) + " " + str(accuracy))
		total_acc += accuracy
	print("Avg Accuracy " + str((total_acc/15)))


def main(args):
	print(args)
	index = int(args[0])
	m = models[index]
	caffe.set_mode_cpu()
	print("test start for: " + m[0] + " " + m[1])
	net = caffe.Net(m[0], m[1], caffe.TEST)
	test_for_model(net)

if __name__ == "__main__":
   main(sys.argv[1:])
