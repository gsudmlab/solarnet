from pylab import *
#matplotlib inline
caffe_root = '/opt/caffe/'  # this file should be run from {caffe_root}/examples (otherwise change this line)

import sys
sys.path.insert(0, caffe_root + 'python')
import caffe

# run scripts from caffe root
import os
# Download data
#print 'data/mnist/get_mnist.sh'
# Prepare data
#print 'examples/mnist/create_mnist.sh'
# back to examples

from caffe import layers as L, params as P
    
#with open('mnist/lenet_auto_train.prototxt', 'w') as f:
#    f.write(str(lenet('mnist/mnist_train_lmdb', 64)))
    
#with open('mnist/lenet_auto_test.prototxt', 'w') as f:
#    f.write(str(lenet('mnist/mnist_test_lmdb', 100)))

#caffe.set_device(1)
caffe.set_mode_cpu()

### load the solver and create train and test nets
#solver = None  # ignore this workaround for lmdb data (can't instantiate two solvers on the same data)
solver = caffe.SGDSolver('/home/ahmet/workspace/solarnet/models/alexnet_modified/solver_default.prototxt')

# each output is (batch size, feature dim, spatial dim)
for k, v in solver.net.blobs.items():
    print (k, v.data.shape)

# just print the weight sizes (we'll omit the biases)
for k, v in solver.net.params.items():
    print (k, v[0].data.shape)

solver.solve()  # train net

for x in range(0, 4):
	print 'test starts'
	solver.test_nets[0].forward()  # test net (there can be more than one)
	
	# we use a little trick to tile the first eight images
	#imshow(solver.net.blobs['data'].data[:8, 0].transpose(1, 0, 2).reshape(28, 8*28), cmap='gray'); axis('off')
	#print 'actual labels:', solver.net.blobs['label'].data.astype(int)
	#print 'predicted labels:', solver.net.blobs['prob'].data.argmax(1)

	labels = solver.test_nets[0].blobs['label'].data.astype(int)
	predictions = solver.test_nets[0].blobs['prob'].data.argmax(1)
	probs = solver.test_nets[0].blobs['prob'].data

	labelMap = ['SG', 'CH', 'AR', 'FL','U']

	for l,p, pp in zip(labels,predictions, probs):
		#if l != p:
		print 'actual: ', labelMap[l], ' predicted: ', labelMap[p], ' probs: ', pp[p], ' - ', pp[0], ',', pp[1], ',', pp[2], ',', pp[3], ',', pp[4]

	print 'accuracy : ', str(sum(solver.test_nets[0].blobs['label'].data == solver.test_nets[0].blobs['prob'].data.argmax(1)))
#print 'accuracy : ', str(sum(solver.test_nets[0].blobs['label'].data == solver.test_nets[0].blobs['ip2'].data.argmax(1))) 

##imshow(solver.test_nets[0].blobs['data'].data[:8, 0].transpose(1, 0, 2).reshape(28, 8*28), cmap='gray'); axis('off')
#print 'test labels:', solver.test_nets[0].blobs['label'].data[:]
