class AccuracyUtil(object):
    def __init__(self):
        self.missCountMap = {}
        self.hitCountMap = {}
	self.confusion = {}
	self.listOfEvent = ['SG', 'CH', 'AR', 'FL', 'QS']
	for e in self.listOfEvent:
		self.missCountMap[e] = 0
		self.hitCountMap[e] = 0
		self.confusion[e] = {}
		for e2 in self.listOfEvent:
			self.confusion[e][e2] = 0

    def addToRight(self, eventType):
	eventType = eventType.strip()
	self.hitCountMap[eventType] = self.hitCountMap[eventType] + 1

    def addToWrong(self, eventType, wrongType):
    	eventType = eventType.strip()
	wrongType = wrongType.strip()
	self.missCountMap[eventType] = self.missCountMap[eventType] + 1
	self.confusion[eventType][wrongType] = self.confusion[eventType][wrongType] + 1

    def printAccuracy(self):
        print self.confusion
	for e in self.listOfEvent:
            hitCount = self.hitCountMap[e]
            missCount = self.missCountMap[e]
	    confusedWith = self.confusion[e]
            print 'For ', e, ' Hit: ', hitCount, ' Miss: ', missCount, ' Accuracy: ', (hitCount * 100) / (hitCount+missCount), ' Error: ', (missCount * 100) / (hitCount+missCount)
	    print 'Confusion ', confusedWith

f = AccuracyUtil()
with open("output_new_200.txt", "r") as ins:
    for line in ins:
        tuples = line.split('\t')
        if len(tuples) > 3:
            if tuples[1] == tuples[0]:
                f.addToRight(tuples[0])
            else:
                f.addToWrong(tuples[0],tuples[1])
                if tuples[0] == 'CH':
                    print tuples[1]
    f.printAccuracy()
        	
