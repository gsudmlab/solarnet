import caffe
import numpy as np
from PIL import Image
import sys
from tile_image import SolarImageLabeler

root = '/home/ahmet/workspace/solarnet/deploy/'
image_size = 4096
patch_size = 512
input_image_size = 256

caffe.set_mode_cpu();

def get_model_by_name(name):
	if name == "lenet":
		model = root + 'model/lenet/deploy_32.prototxt';
		weights = root + 'model/lenet/_iter_1000.caffemodel';
		input_image_size = 32
		return model, weights
	if name == "googlenet":
		model = root + 'model/googlenet/deploy_256.prototxt';
		weights = root + 'model/googlenet/_iter_5000.caffemodel';
		input_image_size = 224
		return model, weights
	if name == "cifar":
		model = root + 'model/cifar/deploy_256.prototxt';
		weights = root + 'model/cifar/_iter_5000.caffemodel';
		input_image_size = 256
		return model, weights

	return "", ""

model, weights = get_model_by_name("cifar")
net = caffe.Net(model, weights, caffe.TEST);

def resize_image(im, w, h):
	im = Image.fromarray(im)
	im = im.resize((w, h), Image.ANTIALIAS)
	return np.array(im)

def read_proto_as_array(filename):
	blob = caffe.proto.caffe_pb2.BlobProto()
	data = open( filename , 'rb' ).read()
	blob.ParseFromString(data)
	arr = np.array( caffe.io.blobproto_to_array(blob) )
	return arr


def get_transformer():
	transformer = caffe.io.Transformer({'data': net.blobs['data'].data.shape})

	#load the mean ImageNet image (as distributed with Caffe) for subtraction
	mu = read_proto_as_array(root + 'data/imagenet_mean.binaryproto')
	print(mu.shape)
	mu = mu.mean(0).mean(1).mean(1)  # average over pixels to obtain the mean (BGR) pixel values
	print(mu.shape)
	print 'mean-subtracted values:', zip('BGR', mu)


	transformer.set_transpose('data', (2,0,1))  # move image channels to outermost dimension
	transformer.set_mean('data', mu)            # subtract the dataset-mean value in each channel
	transformer.set_raw_scale('data', 255)      # rescale from [0, 1] to [0, 255]
	transformer.set_channel_swap('data', (2,1,0)) 
	return transformer
	#net.blobs['data'].reshape(1,3,256,256)

transformer = get_transformer()
	
def find_label(image):
	transformed_image = transformer.preprocess('data', image)
	#image = np.transpose(image, (2, 0, 1))
	#image = np.expand_dims(image, axis=0)
	net.blobs['data'].data[...] = transformed_image

	output = net.forward()

	output_prob = output['prob'][0]  # the output probability vector for the first image in the batch
	print 'predicted probs: ', output_prob
	labels = ['SG', 'CH', 'AR', 'FL','QS']
	print 'predicted class index is:', output_prob.argmax()
	print 'predicted class label is:', labels[output_prob.argmax()]
	if output_prob.max() < 0.95:
		return "QS"
	return labels[output_prob.argmax()]


def main(args):
	
	imagepath = args[0]
	outputimagepath = args[1]
	labeler = SolarImageLabeler(imagepath, patch_size)
	patch_count = int(image_size/patch_size)

	for i in range(patch_count):
		for j in range(patch_count):
			patch_image = labeler.get_patch(i,j)
			input_image = resize_image(patch_image, input_image_size, input_image_size)
			label = find_label(input_image)
			if label != "QS":
				labeler.add_label(i,j,label)
	labeler.save_fig(outputimagepath)

if __name__ == "__main__":
	main(sys.argv[1:])
