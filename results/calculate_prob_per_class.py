eventByWrong = {}
eventByRight = {}

def addToRight(eventType):
	if eventType in eventByRight:
		eventByRight[eventType] = eventByRight[eventType] + 1
	else:
		eventByRight[eventType] = 1

def addToWrong(eventType):
	if eventType in eventByWrong:
		eventByWrong[eventType] = eventByWrong[eventType] + 1
	else:
		eventByWrong[eventType] = 1

with open("alexnet_iteration_1000_data_500_probs.txt", "r") as ins:
    for line in ins:
    	tuples = line.split('\t')
        if len(tuples) > 3:
        	if tuples[1] == tuples[0]:
        		addToRight(tuples[0])
        	else:
        		addToWrong(tuples[0])
        		if tuples[0] == 'CH':
        			print tuples[1]

print 'right: ', eventByRight
print 'wrong: ', eventByWrong

        	
